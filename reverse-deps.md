# Reverse Dependencies

After modification/rebuild of this project, it is necessary to rebuild also the following projects:

*	https://gitlab.com/rychly-edu/docker/docker-spark -- the same Alpine version as in https://github.com/AdoptOpenJDK/openjdk-docker/blob/master/8/jdk/alpine/Dockerfile.hotspot.releases.slim

The projects above may require particular version of the PyArrow built on particular version of Alpine linux.
