FROM alpine

COPY scripts/01_* /

RUN true \
&& chmod 755 /01_*.sh \
&& /01_install-build-env.sh

ARG PYTHON_VER="3"

COPY scripts/02_* /

RUN true \
&& chmod 755 /02_*.sh \
&& /02_install-python-env.sh ${PYTHON_VER}

COPY scripts/03_* /

RUN true \
&& chmod 755 /03_*.sh \
&& /03_install-pip-env.sh ${PYTHON_VER}

COPY scripts/95_* /

ARG ARROW_VER="0.14.1"
ARG ARROW_BUILD_TYPE="release"

RUN true \
&& chmod 755 /95_*.sh \
&& /95_build.sh ${PYTHON_VER} ${ARROW_VER} ${ARROW_BUILD_TYPE}

COPY scripts/96_* /

RUN true \
&& chmod 755 /96_*.sh \
&& /96_dist.sh ${PYTHON_VER} /tmp/dist
