#!/bin/sh
# adapted from https://gist.github.com/bskaggs/fc3c8d0d553be54e2645616236fdc8c6 and https://github.com/apache/arrow/issues/2818

PYTHON_VER=${1:-3}

set -e	# Exit immediately if a command exits with a non-zero status.

## READY

pip${PYTHON_VER} install --no-cache-dir cython
pip${PYTHON_VER} install --no-cache-dir pandas
