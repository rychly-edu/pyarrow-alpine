#!/bin/sh
# adapted from https://gist.github.com/bskaggs/fc3c8d0d553be54e2645616236fdc8c6 and https://github.com/apache/arrow/issues/2818

PYTHON_VER=${1:-3}
ARROW_VER=${2:-0.12.0}	# see https://github.com/apache/arrow/releases
ARROW_BUILD_TYPE=${3:-release}
ARROW_DIR=/tmp/apache-arrow

set -e	# Exit immediately if a command exits with a non-zero status.

## READY

# 01_install-build-env.sh
# 02_install-python-env.sh
# 03_install-pip-env.sh

## STEADY

rm -rf "${ARROW_DIR}" || true
mkdir -p "${ARROW_DIR}"
wget -O - "https://github.com/apache/arrow/archive/apache-arrow-${ARROW_VER}.tar.gz" | tar -xz -C "${ARROW_DIR}" --strip-components 1
mkdir -p "${ARROW_DIR}/cpp/build"

## GO

cd "${ARROW_DIR}/cpp/build"

ARROW_HOME=/usr/local
PARQUET_HOME=/usr/local

cmake \
-DCMAKE_BUILD_TYPE=${ARROW_BUILD_TYPE} \
-DCMAKE_INSTALL_LIBDIR=lib \
-DCMAKE_INSTALL_PREFIX=${ARROW_HOME} \
-DARROW_PARQUET=on \
-DARROW_PYTHON=on \
-DARROW_PLASMA=on \
-DARROW_BUILD_TESTS=OFF \
..

make -j$(nproc)
make install

cd "${ARROW_DIR}/python"

python${PYTHON_VER} setup.py build_ext --build-type=${ARROW_BUILD_TYPE} --with-parquet
python${PYTHON_VER} setup.py install
