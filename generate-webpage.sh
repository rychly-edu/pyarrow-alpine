#!/bin/sh

IFS=$'\n'
PUBLIC=${1:-build}

function makeLink() {
	SPACE=${1}
	shift
	for I in $*; do
		BASENAME=${I##*/}
		echo "${SPACE}<li><a href=\"${I}\">${BASENAME}</a></li>"
	done
}

cd "${PUBLIC}"

cat <<END
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Apache Arrow and PyArrow for Alpine</title>
	<link rel="stylesheet" href="https://www.google.com/css/maia.css">
</head>
<body>
	<h1>Apache Arrow and PyArrow for Alpine</h1>
	<p>
		<a href="${CI_PROJECT_URL}/tree/${CI_COMMIT_REF_NAME}">${CI_COMMIT_REF_NAME}</a> build
		on <emph>$(date -R)</emph>,
		commit <a href="${CI_PROJECT_URL}/commit/${CI_COMMIT_SHA}">${CI_COMMIT_SHA}</a>
	</p>
	<h2>Distribution</h2>
	<ul>
$(makeLink "		" ./distributions/*)
	</ul>
	<h2>License</h2>
	<pre>$(cat LICENSE | tr -d '<>&')</pre>
</body>
</html>
END
