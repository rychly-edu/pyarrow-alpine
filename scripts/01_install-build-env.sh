#!/bin/sh
# adapted from https://gist.github.com/bskaggs/fc3c8d0d553be54e2645616236fdc8c6 and https://github.com/apache/arrow/issues/2818

set -e	# Exit immediately if a command exits with a non-zero status.

## READY

apk add --no-cache \
build-base \
cmake \
bash \
boost-dev \
autoconf \
zlib-dev \
flex \
bison

# jemalloc-dev is not available in Alpine 3.9 and above, so the latest available is in Alpine 3.8, see https://pkgs.alpinelinux.org/packages?name=jemalloc&branch=v3.8
apk search --no-cache -q jemalloc-dev | grep -q '' \
&& apk add --no-cache jemalloc-dev \
|| true
