#!/bin/sh
# adapted from https://gist.github.com/bskaggs/fc3c8d0d553be54e2645616236fdc8c6 and https://github.com/apache/arrow/issues/2818

PYTHON_VER=${1:-3}
TARGET_DIR=${2:-/tmp/dist}
ARROW_DIR=/tmp/apache-arrow

set -e	# Exit immediately if a command exits with a non-zero status.

## READY

# 01_install-build-env.sh
# 02_install-python-env.sh
# 03_install-pip-env.sh

## STEADY && GO

# 95_build.sh

## FINISH

cd "${ARROW_DIR}/python"

# see https://docs.python.org/2/distutils/builtdist.html
python${PYTHON_VER} setup.py bdist --formats=gztar

cd "${ARROW_DIR}/python/dist"

ARROW_NAME=$(ls *.tar.gz | head -1 | sed "s/pyarrow/python${PYTHON_VER}-arrow/")

mkdir "${TARGET_DIR}"
mv -v *.egg "${TARGET_DIR}"
for I in *.tar.gz; do
	mv -v ${I} "${TARGET_DIR}/python${PYTHON_VER}-${I}"
done

ARROW_HOME=/usr/local

tar czf "${TARGET_DIR}/${ARROW_NAME}" /usr/local

cd "${TARGET_DIR}"

ALPINE_VER=$(cat /etc/alpine-release)

for I in *; do
	mv "${I}" "alpine${ALPINE_VER}-${I}"
done
