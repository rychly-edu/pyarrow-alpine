#!/bin/sh
# adapted from https://gist.github.com/bskaggs/fc3c8d0d553be54e2645616236fdc8c6 and https://github.com/apache/arrow/issues/2818

PYTHON_VER=${1:-3}

set -e	# Exit immediately if a command exits with a non-zero status.

## READY

apk add --no-cache \
python${PYTHON_VER}-dev \
py${PYTHON_VER}-pip \
py${PYTHON_VER}-six \
py${PYTHON_VER}-pytest \
py${PYTHON_VER}-numpy \
py-numpy-dev \
py${PYTHON_VER}-dateutil \
py${PYTHON_VER}-tz

# make Python compatible with manylinux wheels and create some useful symlinks that are expected to exist for default Python version
# see https://github.com/insightfulsystems/alpine-python and https://issues.apache.org/jira/browse/ARROW-2058
PYTHON_VER2=$(python${PYTHON_VER} --version 2>&1 | sed 's/^.*\([0-9]\+\.[0-9]\+\)\..*$/\1/')
echo "manylinux1_compatible = True" > /usr/lib/python${PYTHON_VER2}/_manylinux.py
ln -sf locale.h /usr/include/xlocale.h
ln -sf easy_install-${PYTHON_VER2} /usr/bin/easy_install
ln -sf idle${PYTHON_VER2} /usr/bin/idle
ln -sf pydoc${PYTHON_VER2} /usr/bin/pydoc
ln -sf python${PYTHON_VER2} /usr/bin/python
ln -sf python-config${PYTHON_VER2} /usr/bin/python-config
ln -sf pip${PYTHON_VER2} /usr/bin/pip
