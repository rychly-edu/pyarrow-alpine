# Apache Arrow and PyArrow for Alpine

Website: https://rychly-edu.gitlab.io/pyarrow-alpine/ (for builds, license, etc.)

Some code was adapted from https://gist.github.com/bskaggs/fc3c8d0d553be54e2645616236fdc8c6 and https://github.com/apache/arrow/issues/2818.

## Building with Docker

~~~sh
cd pyarrow-alpine
docker build --pull -t pyarrow-alpine .
docker run --rm -ti pyarrow-alpine
# ls /tmp/dist
~~~

## Usage

Builds for Python 2 and for Python 3 are not compatible,
e.g., in the case of using the Python 3 build with Python 2, there is missing symbol PyUnicode_AsEncodedString in libarrow_python.so.14.

### Python 3

~~~sh
cd /
## Arrow
# ldd /usr/local/lib/libarrow.so: boost-filesystem
# ldd /usr/local/lib/libparquet.so: boost-regex (libstdc++ icu-libs)
# LD_PRELOAD=/usr/lib/libpython3.so ldd /usr/local/lib/libarrow_python.so: NONE
apk add --no-cache boost-filesystem boost-regex
tar xzf /tmp/dist/alpine*-python3-arrow-*.linux-x86_64.tar.gz
## PyArrow in Python3
apk add --no-cache py3-six py3-numpy
easy_install-3.7 /tmp/dist/alpine*-pyarrow-*-py3.7-linux-x86_64.egg
~~~

### Python 2

~~~sh
cd /
## Arrow
# ldd /usr/local/lib/libarrow.so: boost-filesystem
# ldd /usr/local/lib/libparquet.so: boost-regex (libstdc++ icu-libs)
# LD_PRELOAD=/usr/lib/libpython2.so ldd /usr/local/lib/libarrow_python.so: NONE
apk add --no-cache boost-filesystem boost-regex
tar xzf /tmp/dist/alpine*-python2-arrow-*.linux-x86_64.tar.gz
## PyArrow in Python2
apk add --no-cache py2-futures py-enum34 py2-six py2-numpy
easy_install-2.7 /tmp/dist/alpine*-pyarrow-*-py2.7-linux-x86_64.egg
~~~
